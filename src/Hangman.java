import java.util.Scanner;

public class Hangman {
    private static final String[] mysteriousSomeone = {"JAROSŁAW", "DONALD", "ADRIAN"};
    private static final int NumberOfMistakes = 9;

    public static void main(String[] args) {
        String word = chooseRandomWord();
        char[] guesstheLetters = new char[word.length()];
        int numErrors = 0;
        String[] drawHangman = {
                "||__________________", "||", "||        |", "||      (|", "||      (oo)", "||      (||)",
                "||       |", "||      _|", "||      _||", "||      _||_", "||_|_|_|_|_|_|_|_|_|"
        };

        Scanner scanner = new Scanner(System.in);

        while (true) {
            displayGameStatus(guesstheLetters, numErrors);

            System.out.print("Podaj jedną DUŻĄ literę :");
            char selectedLetter = scanner.next().charAt(0);

            if (whetherLetterAlreadyGuessed(selectedLetter, guesstheLetters)) {
                System.out.println("Ta litera już była. Spróbuj jeszcze raz.");
                continue;
            }
            if (whetherLetterAppears(selectedLetter, word)) {
                updateLetters(selectedLetter, word, guesstheLetters);
            } else {
                System.out.println("Porażka ! myśl dalej!");
                System.out.println("><><><><><><><><><><><><><><><><><><><><><><><><><><");
                numErrors++;
            }
            if (ifHeLost(numErrors)) {
                System.out.println("Przegrałeś! Niedługo odwiedzi Cię: " + word+"!!!");
                System.out.println("><><><><><><><><><><><><><><><><><><><><><><>");
                break;
            }
            if (ifHeWin(guesstheLetters)) {
                System.out.println("Brawo !!! szykuj się !! lepiej było zawisnąć niż spotkać się z " + word+"EM");
                System.out.println("><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>");
                break;
            }
            if (numErrors > 0) {
                switch (numErrors) {
                    case 1:
                        System.out.println(drawHangman[0]);
                        System.out.println(drawHangman[2]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[10]);
                        break;
                    case 2:
                        System.out.println(drawHangman[0]);
                        System.out.println(drawHangman[2]);
                        System.out.println(drawHangman[4]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[10]);
                        break;
                    case 3:
                        System.out.println(drawHangman[0]);
                        System.out.println(drawHangman[2]);
                        System.out.println(drawHangman[4]);
                        System.out.println(drawHangman[3]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[10]);
                        break;
                    case 4:
                        System.out.println(drawHangman[0]);
                        System.out.println(drawHangman[2]);
                        System.out.println(drawHangman[4]);
                        System.out.println(drawHangman[5]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[10]);
                        break;
                    case 5:
                        System.out.println(drawHangman[0]);
                        System.out.println(drawHangman[2]);
                        System.out.println(drawHangman[4]);
                        System.out.println(drawHangman[5]);
                        System.out.println(drawHangman[6]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[10]);
                        break;
                    case 6:
                        System.out.println(drawHangman[0]);
                        System.out.println(drawHangman[2]);
                        System.out.println(drawHangman[4]);
                        System.out.println(drawHangman[5]);
                        System.out.println(drawHangman[7]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[10]);
                        break;
                    case 7:
                        System.out.println(drawHangman[0]);
                        System.out.println(drawHangman[2]);
                        System.out.println(drawHangman[4]);
                        System.out.println(drawHangman[5]);
                        System.out.println(drawHangman[8]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[10]);
                        break;
                    case 8:
                        System.out.println(drawHangman[0]);
                        System.out.println(drawHangman[2]);
                        System.out.println(drawHangman[4]);
                        System.out.println(drawHangman[5]);
                        System.out.println(drawHangman[9]);
                        System.out.println(drawHangman[1]);
                        System.out.println(drawHangman[10]);
                        break;
                    default:
                        break;
                }
            }
        }
        scanner.close();
    }
    private static String chooseRandomWord() {
        int indeks = (int) (Math.random() * mysteriousSomeone.length);
        return mysteriousSomeone[indeks];
    }
    private static void displayGameStatus(char[] guesstheLetters, int numErrors) {
        System.out.println("><><><><><><><><><><><><><><><><><><><><><><><><><><");
        System.out.println("Wyraz składa się z DUŻYCH liter !");
        System.out.println("Zgadnij imię ! Tajemniczy ktoś to : <><>" + new String(guesstheLetters) +"."+"<><>");
        System.out.println("Liczba chybień : " + numErrors);
    }
    private static boolean whetherLetterAlreadyGuessed(char letter, char[] guesstheLetters) {
        for (char guessLetter : guesstheLetters) {
            if (letter == guessLetter) {
                return true;
            }
        }
        return false;
    }
    private static boolean whetherLetterAppears(char letter, String word) {
        return word.indexOf(letter) != -1;
    }
    private static void updateLetters(char letter, String word, char[] guesstheLetters) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == letter) {
                guesstheLetters[i] = letter;
            }
        }
    }
    private static boolean ifHeLost(int numErrors) {
        return numErrors >= NumberOfMistakes;
    }
    private static boolean ifHeWin(char[] guesstheLetters) {
        for (char letter : guesstheLetters) {
            if (letter == 0) {
                return false;
            }
        }
        return true;
    }
}







